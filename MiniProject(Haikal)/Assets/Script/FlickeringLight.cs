﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlickeringLight : MonoBehaviour {

	Light light;
	public float minTime;
	public float maxTime;

	// Use this for initialization
	void Start () {
		light = GetComponent<Light> ();
		StartCoroutine (Flashing ());

	}
	IEnumerator Flashing()
	{
		while (true) {
			yield return new WaitForSeconds (Random.Range (minTime, maxTime));
			light.enabled = !light.enabled;
		}
	}
}
