﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GirlScrae : MonoBehaviour {

	public GameObject Girl5;
	public GameObject Girl4;
	public GameObject Girl3;
	public GameObject Girl2;
	public GameObject Girl1;

	public bool Triggered;

	public AudioSource AudioScream;
	public AudioClip Scream;

	float wait4Scream = 2.2f;
	float time = 0.1f;
	float end = 1.5f;

	// Use this for initialization
	void Start () {
		Triggered = false;
		Girl5.SetActive (false);
		Girl4.SetActive (false);
		Girl3.SetActive (false);
		Girl2.SetActive (false);
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag ("Player") && Triggered == false) {
			Triggered = true;
			StartCoroutine (MoveForwardScare());
		}
	}

	IEnumerator MoveForwardScare()
	{
		AudioScream.Play ();
		yield return new WaitForSeconds (wait4Scream);
		Girl2.SetActive (true);
		yield return new WaitForSeconds (time);
		Girl1.SetActive (false);
		Girl3.SetActive (true);
		yield return new WaitForSeconds (time);
		Girl2.SetActive (false);
		Girl4.SetActive (true);
		yield return new WaitForSeconds (time);
		Girl3.SetActive (false);
		Girl5.SetActive (true);
		yield return new WaitForSeconds (time);
		Girl4.SetActive (false);
		yield return new WaitForSeconds (end);
		Girl5.SetActive (false);
	}

	
	// Update is called once per frame
	void Update () {
		AudioScream.clip = Scream;
	}
}
