﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TvScare : MonoBehaviour {

	public AudioSource TvSound;

	void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag ("Player")) {
			TvSound.Play ();
		}

	}
	// Update is called once per frame
	void Update () {
		
	}
}
