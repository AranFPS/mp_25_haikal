﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flashlight : MonoBehaviour {

	public AudioClip soundOn;
	public AudioClip soundOff;

	public AudioSource source;

	public Light flashlight;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.F)) {
			if (flashlight.enabled == false) {
				flashlight.enabled = true;
				source.clip = soundOn;
				source.Play ();
			} else {
				flashlight.enabled = false;
				source.clip = soundOff;
				source.Play ();
			}
		}
	}
}
